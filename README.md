# Blueprints Landing Page
## Introduction
SOAFEE Blueprints is an initiative driven by SOAFEE's members to materialize the concepts behind SOAFEE's vision of how to enable the software defined vehicle (SDV). This is done by deploying domain-specific applications in a way which leverage features (e.g., containerization) which SOAFEE envisions necessary to realize the SDV.

One important aspect of a blueprint is for a user to be able to reproduce the blueprint on their end. This requires well written and very clear documentation. A user can consume a blueprint as is, for inspiration or as a foundation on which to make modifications which suit their needs.

## Structure
A blueprint consists of the following:
* domain-specific application(s)
* a SOAFEE compliant implementation
* standards-based firmware
* hardware; edge, cloud and/or virtual

<img src="./images/bp_generic.jpg" alt="bp_generic" width="600"/>

## Best Practices
Placeholder

## Tracker
| Release Date | Blueprint | Release |
| ------------ | --------- | ------- |
| 2022 October | [Autoware Open AD Kit](autoware_open_ad_kit_blueprint/README.md) | [R1](autoware_open_ad_kit_blueprint/r1/README.md) |